package org.sbt.devops;

import java.util.HashSet;

/**
 * Created by SBT-Kamalov-AN on 12.09.2017.
 */
public class BuildVars {

    public static final String pathDB = "hsqldb/";
    public static final String nameDB = "telegram-bot_db";
    public static final String userDB = "SA";
    public static final String passwordDB = "";
    public static final String FILE_PATH = "DataPIR.xml";

    private static HashSet<Integer> USERS = new HashSet<>();

    public static HashSet<Integer> getUSERS() {
        if (USERS.isEmpty())
            setUSERS();
        return USERS;
    }

    private static void setUSERS() {
        USERS.add(351894279); //kamalov
        USERS.add(335820489); //belov
        USERS.add(260693742); //kuznetsov
        USERS.add(200219855); //grigoriev
        USERS.add(274623019); //voznenko
        USERS.add(50338861); //kolchinskiy
        USERS.add(306686931); //domarev
        USERS.add(426433947); //shvakov
        USERS.add(118968778); //balutina
        USERS.add(307003636); //spirenkov
        USERS.add(168809929); //devyatov
    }
}
